clc;

% началые условия 
E = [0 0; 0 1];
A = [1 0; 1 -3];
B = [1; 0];
Bd = [0.1; 0.2];
C = [2 1; -1 2];
D = [0; 1];

% размерности матриц
n = size(A, 1);
m = size(B, 2);
p = size(C, 1);
q = size(D, 2);
r = size(Bd, 2);

Eb = [E zeros(n, q); zeros(q, n) eye(q)];
Ab = [A zeros(n, q); zeros(q, n) zeros(q, q)];
Bb = [B; zeros(q, m)];
Gb = [Bd zeros(n, q); zeros(q, r) eye(q)];
Cb = [C D];

LD = [0.1 0; 0 0.1; 5 10];

Sb = Eb + LD * Cb;

% скорость обучения
Gamma = 100;

% константа Липшица
Lf = 0.1347;

% веса матрица H1 и H2
H1 = eye(n + q);
H2 = eye(m);

% инициализация линейного матричного неравенства
setlmis([]);

% задание переменных систем
P = lmivar(1, [3, 1]);
Pb = lmivar(1, [3, 1]);
varepsilon = lmivar(1, [1 0]);
varepsilonb = lmivar(1, [1 0]);
Y = lmivar(2, [3, 2]);
F1 = lmivar(2, [1 2]);
F2 = lmivar(2, [1 2]);
mu = lmivar(1, [1 0]);

% задание членов системы
lmiterm([1 1 1 P], 1, inv(Sb) * Ab);
lmiterm([1 1 1 P], Ab' * inv(Sb)', 1);
lmiterm([1 1 1 Y], 1, -Cb, 's');
lmiterm([1 1 1 P], 1, 1);
lmiterm([1 1 1 0], H1' * H1);
lmiterm([1 2 1 P], Bb' * inv(Sb)',1);
lmiterm([1 2 1 F2], 1, -Cb * inv(Sb) * Ab);
lmiterm([1 2 1 F1], 1, -Cb);
lmiterm([1 2 2 F2], 1, -Cb * inv(Sb) * Bb);
lmiterm([1 2 2 -F2], Bb' * inv(Sb)' * Cb', -1);
lmiterm([1 2 2 0], inv(Gamma) + H2' * H2);
lmiterm([1 3 1 P], Gb' * inv(Sb)', 1);
lmiterm([1 3 2 -F2], -Gb' * inv(Sb)' * Cb', 1);
lmiterm([1 3 3 mu], 1, -eye(2));
lmiterm([1 4 1 0], zeros(1, 3));
lmiterm([1 4 2 0], inv(Gamma));
lmiterm([1 4 3 0], zeros(1, 2));
lmiterm([1 4 4 mu], 1, -1);
lmiterm([1 5 1 0], zeros(3, 3));
lmiterm([1, 5 2 -F2], Cb', 1);
lmiterm([1 5 3 0], zeros(3, 2));
lmiterm([1 5 4 0], zeros(3, 1));
lmiterm([1 5 5 varepsilon], 1, -eye(3));
lmiterm([1 6 1 Y], 1, Cb);
lmiterm([1 6 2 0], zeros(3, 1));
lmiterm([1 5 3 0], zeros(3, 2));
lmiterm([1 6 4 0], zeros(3, 1));
lmiterm([1 6 5 0], zeros(3, 3));
lmiterm([1 6 6 varepsilonb],1, -eye(3));

lmiterm([-2 1 1 P], 1, 1);
lmiterm([-2 2 1 0], eye(3));
lmiterm([-2 2 2 Pb], 1, 1);

lmiterm([-3 1 1 varepsilon], 1, 1);
lmiterm([-3 2 1 0], 1);
lmiterm([-3 2 2 varepsilonb], 1, 1);

lmiterm([-4 1 1 P], 1, 1);

lmiterm([-5 1 1 varepsilon], 1, 1);

lmiterm([-6 1 1 varepsilonb], 1, 1);

lmiterm([-7 1 1 0], eye(3));
lmiterm([-7 2 1 Pb], 1, 1);
lmiterm([-7 2 2 0], eye(3));

% представление системы
example1 = getlmis;

% решение системы
[tmin, feas] = feasp(example1);

PP = dec2mat(example1, feas, P);
PbP = dec2mat(example1, feas, Pb);
varepsilonP = dec2mat(example1, feas, varepsilon);
varepsilonbP = dec2mat(example1, feas, varepsilonb);
YP = dec2mat(example1, feas, Y);
F1P = dec2mat(example1, feas, F1);
F2P = dec2mat(example1, feas, F2);
muP = dec2mat(example1, feas, mu);

n = decnbr(example1);

xinit = mat2dec(example1, PP, PbP, varepsilonP, varepsilonP, YP, F1P, F2P, muP);

% итерация алгортма 
for i=1:200
    c = zeros(n, 1);
    for j=1:n
        [Pj, Pbj, varepsilonj, varepsilonbj, Yj, F1j, F2j, muj] = defcx(example1, j, P, Pb, varepsilon, varepsilonb, Y, F1, F2, mu);
        c(j) = trace(PP * Pbj + PbP * Pj + varepsilonP  * varepsilonbj * eye(3) + varepsilonbP * varepsilonj * eye(3) + muj * eye(3));
    end
    
    options = [1e-5, 0, 0, 0, 0];
    [copt, xopt] = mincx(example1, c, options, xinit, []);
    
    xinit = mat2dec(example1, PP, PbP, varepsilonP, varepsionbP, YP, F1P, F2P, muP);
    
    PPP = dec2mat(example1, xopt, P);
    PbPP = dec2mat(example1, xopt, Pb);
    
    varepsilonPP = dec2mat(example1, xopt, varepsilon);
    varepsilonbPP = dec2mat(example1, xopt, varepsilonb);
    YPP = dec2mat(example1, xopt, Y);
    
    F1PP = dec2mat(example1, xopt, F1);
    F2PP = dec2mat(example1, xopt, F2);
    muPP = dec2mat(example1, xopt, mu);
    
    z11 = PPP * inv(Sb) * Ab + Ab' * inv(Sb)' * PPP - YPP * Cb - Cb' * YPP' + PPP + H1' * H1;
    z12 = PPP * inv(Sb) * Bb - Ab' * inv(Sb)' * Cb' * F2PP' - Cb' * F1PP';
    z13 = PPP * inv(Sb) * Gb;
    z14 = zeros(3, 1);
    z15 = zeros(3, 3);
    z16 = Cb' * YPP';
    z22 = -F2PP * Cb * inv(Sb) * Bb - Bb' * inv(Sb)' * Cb' * F2PP' + inv(Gamma) + H2' * H2;
    z23 = -F2PP * Cb * inv(Sb) * Gb;
    z24 = inv(Gamma);
    z25 = F2PP * Cb;
    z26 = zeros(1, 3);
    z33 = -muPP * eye(2);
    z34 = zeros(2, 1);
    z35 = zeros(2, 3);
    z36 = zeros(2, 3);
    z44 = -muPP;
    z45 = zeros(1, 3);
    z46 = zeros(1, 3);
    z55 = -varepsilonPP * eye(3);
    z56 = zeros(3, 3);
    z66 = -inv(varepsilonPP) * eye(3);
    
    Z = [z11 z12 z13 z14 z15 z16
         z12' z22 z23 z24 z25 z26
         z13' z23' z33 z34 z35 z36
         z14' z24' z34' z44 z45 z46
         z15' z25' z35' z45' z55 z56
         z16' z26' z36' z46' z56' z66];
     
     M1 = [eig(Z); eig(inv(PPP) * inv(PPP) - eye(3))];
     i2 = 0;
     for i1=1:length(M1)
       if (M1(i1, 1) < 0)
           i2 = i2 + 1;
       end
     end
     
     PP = PPP;
     PbP = PbPP
     varepsilonP = varepsilonPP;
     varepsilonbP = varepsilonbPP;
     YP = YPP;
     F1P = F1PP;
     F2P = F2PP;
     muP = muPP;
     
     if i2 == length(M1)
         break;
     end
end

% не удалось найти решение
if (i == 200)
    disp('there is no result');
end

% решение
P = PP
Pb = PbP
varepsilon = varepsilonP
varepsilonb = varepsilonbP
Y = YP
F1 = F1P
F2 = F2P
mu = muP
LP = Sb * inv(P) * Y

% максимальная ошибка
maxError = 1e-5;
multipliedP = P * Pb;

% проверка условия varpsion * varepsionbP == 1 (с учетом ошибки maxError)
assert(varepsilon * varepsilonbP - 1 < maxError, 'varepsilon should equal 1 / varepsilonbP');

% проверка условия P * Pb == eye(3) (с учетом ошибки maxError)
for i = 1:3
    for j = 1:3
        value = 0;
        if i == j
            value = 1;
        end
        assert(abs(multipliedP(i, j) - value) < maxError, 'P * Pb should equal eye(3)');
    end
end



     